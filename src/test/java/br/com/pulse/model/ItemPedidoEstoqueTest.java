package br.com.pulse.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class ItemPedidoEstoqueTest {

    @Test
    public void deveAdicionarQuantidade() {
        ItemPedidoEstoque itemPedidoEstoque = new ItemPedidoEstoque();
        itemPedidoEstoque.setQuantidade(0);

        itemPedidoEstoque.adicionaQuantidade(10);

        Assertions.assertThat(itemPedidoEstoque.getQuantidade()).isEqualTo(10);
    }

}