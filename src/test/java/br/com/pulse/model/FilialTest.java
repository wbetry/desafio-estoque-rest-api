package br.com.pulse.model;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FilialTest {

    @Test
    public void deveTrazerCnpjFormatado() {
        final Filial filial = new Filial();
        filial.setCnpj("123");

        final String cnpjFinal = "00000000000123";

        assertThat(filial.getCnpjFormatado()).isEqualTo(cnpjFinal);
    }
}
