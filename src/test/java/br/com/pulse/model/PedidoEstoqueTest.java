package br.com.pulse.model;

import static org.assertj.core.api.Assertions.*;

import org.junit.Test;

import java.util.List;

public class PedidoEstoqueTest {

    @Test
    public void deveAdicionarSomandoParaItensRepetidosNaListaDeItens() {
        Produto produto1 = new Produto();
        produto1.setId(1l);
        Produto produto2 = new Produto();
        produto2.setId(2l);

        PedidoEstoque pedido = new PedidoEstoque();

        final ItemPedidoEstoque item1 = new ItemPedidoEstoque();
        item1.setProduto(produto1);
        item1.setPedido(pedido);
        item1.setQuantidade(10);
        final ItemPedidoEstoque item2 = new ItemPedidoEstoque();
        item2.setProduto(produto1);
        item2.setPedido(pedido);
        item2.setQuantidade(10);
        final ItemPedidoEstoque item3 = new ItemPedidoEstoque();
        item3.setProduto(produto2);
        item3.setPedido(pedido);
        item3.setQuantidade(10);

        pedido.adicionar(item1, item2, item3);

        final List<ItemPedidoEstoque> resultado = pedido.getItens();

        assertThat(resultado).size().isEqualTo(2);
        assertThat(resultado).first().extracting("quantidade").isEqualTo(20);
        assertThat(resultado).element(1).extracting("quantidade").isEqualTo(10);
    }

}
