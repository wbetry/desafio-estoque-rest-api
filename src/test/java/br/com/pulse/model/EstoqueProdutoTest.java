package br.com.pulse.model;

import br.com.pulse.model.exception.EstoqueException;
import br.com.pulse.model.exception.EstoqueInsuficianteException;
import org.junit.Assert;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class EstoqueProdutoTest {

    @Test
    public void deveAdicionarQuantidadeAoEstoque() {
        final EstoqueProduto estoqueProduto = new EstoqueProduto();
        estoqueProduto.setQuantidade(0);

        estoqueProduto.adicionarEstoque(10);

        assertThat(estoqueProduto.getQuantidade()).isEqualTo(10);
    }

    @Test
    public  void naoDeveAdicionarQuantidadeAoEstoqueCasoQuantidadeSejaNegativa() {
        final EstoqueProduto estoqueProduto = new EstoqueProduto();
        estoqueProduto.setQuantidade(0);

        assertThatThrownBy(() -> estoqueProduto.adicionarEstoque(-1)).isInstanceOf(EstoqueException.class);
    }

    @Test
    public void deveRemoverQuantidadeDoEstoque() {
        final EstoqueProduto estoqueProduto = new EstoqueProduto();
        estoqueProduto.setQuantidade(20);

        estoqueProduto.removerEstoque(10);

        assertThat(estoqueProduto.getQuantidade()).isEqualTo(10);
    }

    @Test
    public  void naoDeveRemoverQuantidadeAoEstoqueCasoQuantidadeSejaNegativa() {
        final EstoqueProduto estoqueProduto = new EstoqueProduto();
        estoqueProduto.setQuantidade(10);

        assertThatThrownBy(() -> estoqueProduto.removerEstoque(-1)).isInstanceOf(EstoqueException.class);
    }

    @Test
    public  void naoDeveRemoverQuantidadeCasoQuantidadeSejaMaior() {
        final EstoqueProduto estoqueProduto = new EstoqueProduto();
        estoqueProduto.setQuantidade(10);

        assertThatThrownBy(() -> estoqueProduto.removerEstoque(11)).isInstanceOf(EstoqueInsuficianteException.class);
    }

}
