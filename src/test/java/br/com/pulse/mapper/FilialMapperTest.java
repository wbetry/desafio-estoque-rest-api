package br.com.pulse.mapper;

import br.com.pulse.model.Filial;
import org.apache.ibatis.session.SqlSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@MybatisTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class FilialMapperTest {

    @Autowired
    private SqlSession sqlSession;
    @Autowired
    private FilialMapper filialMapper;

    @Before
    public void setup() {

    }

    @Test
    public void deveInserirFilial() {
        Filial filial = new Filial();
        filial.setCnpj("03995515001309");
        filial.setRazaoSocial("Mateus Supermercados S.a  AAAA");

        filialMapper.insert(filial);

        assertThat(filial.getId()).isEqualTo(1l);
    }

    @Test
    public void deveBuscarTodasAsFiliais() {
        Filial filial = new Filial();
        filial.setCnpj("03995515001309");
        filial.setRazaoSocial("Mateus Supermercados S.a");

        filialMapper.insert(filial);

        List<Filial> filiais = filialMapper.findAll();

        assertThat(filiais).size().isEqualTo(1);
        assertThat(filiais).first().extracting("cnpj").isEqualTo("03995515001309");
        assertThat(filiais).first().extracting("razaoSocial").isEqualTo("Mateus Supermercados S.a");
    }

}

