package br.com.pulse.service;

import br.com.pulse.mapper.EstoqueMapper;
import br.com.pulse.model.Estoque;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.BDDMockito.*;

public class EstoqueServiceTest {

    private EstoqueMapper estoqueMapperMock;

    private EstoqueService estoqueService;

    @Before
    public void setup() {
        estoqueMapperMock = mock(EstoqueMapper.class);
        estoqueService = new EstoqueService(estoqueMapperMock);
    }

    @Test
    public void deveBuscarEstoquePorId() {
        Estoque estoqueEsperado = new Estoque();
        estoqueEsperado.setId(1l);

        given(estoqueMapperMock.findById(1l)).willReturn(Optional.ofNullable(estoqueEsperado));

        Optional<Estoque> estoque = estoqueService.findById(1l);

        assertThat(estoque).contains(estoqueEsperado);
    }
    @Test
    public void deveInserirEstoque() {
        Estoque estoque = new Estoque();
        estoque.setId(1l);

        estoqueService.insert(estoque);

        verify(estoqueMapperMock, times(1)).insert(estoque);

    }


}