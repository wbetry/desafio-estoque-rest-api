package br.com.pulse.service;

import br.com.pulse.mapper.ItemPedidoMapper;
import br.com.pulse.model.ItemPedidoEstoque;
import br.com.pulse.model.PedidoEstoque;
import br.com.pulse.model.Produto;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class ItemPedidoServiceTest {

    private ItemPedidoService itemPedidoService;
    private ItemPedidoMapper itemPedidoMapperMock;
    private ProdutoService produtoServiceMock;

    @Before
    public void setup() {
        itemPedidoMapperMock = mock(ItemPedidoMapper.class);
        produtoServiceMock = mock(ProdutoService.class);
        itemPedidoService = new ItemPedidoService(itemPedidoMapperMock, produtoServiceMock);
    }

    @Test
    public void deveInserirItens() {
        final List<ItemPedidoEstoque> itens = preparaCenarioDeItens();

        itemPedidoService.inserirItens(itens);

        verify(itemPedidoMapperMock, times(1)).inserirItens(itens);
    }

    @Test
    public void atualizarStatus() {
        PedidoEstoque pedidoEstoque = new PedidoEstoque();
        pedidoEstoque.setId(1l);
        Produto produto = new Produto();
        produto.setId(1l);

        ItemPedidoEstoque item = new ItemPedidoEstoque(pedidoEstoque, produto);

        itemPedidoService.atualizarStatus(item);

        verify(itemPedidoMapperMock, times(1)).atualizarStatus(item);
    }

    @Test
    public void itensValidos() {
        // Insere produtos com id 1l e 2l
        final List<ItemPedidoEstoque> itensPesquisa = preparaCenarioDeItens();

        final Produto produtoOk = itensPesquisa.get(0).getProduto();

        when(produtoServiceMock.findAllById(1l, 2l)).thenReturn(Arrays.asList(produtoOk));

        final List<ItemPedidoEstoque> itensResultado = itemPedidoService.itensValidos(itensPesquisa);

        verify(produtoServiceMock, times(1)).findAllById(1l, 2l);

        assertThat(itensResultado.size()).isEqualTo(1);
        assertThat(itensResultado).contains(itensPesquisa.get(0));
    }

    private List<ItemPedidoEstoque> preparaCenarioDeItens() {
        PedidoEstoque pedidoEstoque = new PedidoEstoque();
        pedidoEstoque.setId(1l);
        Produto produto1 = new Produto();
        produto1.setId(1l);
        Produto produto2 = new Produto();
        produto2.setId(2l);

        ItemPedidoEstoque item1 = new ItemPedidoEstoque(pedidoEstoque, produto1);
        ItemPedidoEstoque item2 = new ItemPedidoEstoque(pedidoEstoque, produto2);

        return Arrays.asList(item1, item2);
    }
}