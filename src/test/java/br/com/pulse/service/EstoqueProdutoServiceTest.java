package br.com.pulse.service;

import br.com.pulse.mapper.EstoqueProdutoMapper;
import br.com.pulse.model.Estoque;
import br.com.pulse.model.EstoqueProduto;
import br.com.pulse.model.Produto;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

public class EstoqueProdutoServiceTest {

    private EstoqueProdutoService estoqueProdutoService;
    private EstoqueProdutoMapper estoqueProdutoMapper;
    private Estoque estoque;
    private Produto produto;

    @Before
    public void setup() {
        estoqueProdutoMapper = mock(EstoqueProdutoMapper.class);
        estoqueProdutoService = new EstoqueProdutoService(estoqueProdutoMapper);

        estoque = new Estoque();
        estoque.setId(1l);
        produto = new Produto();
        produto.setId(1l);
    }

    @Test
    public void deveAdiconarEstoqueProduto() {


        EstoqueProduto estoqueProduto = new EstoqueProduto(estoque, produto);
        estoqueProduto.setId(1l);

        estoqueProdutoService.inserir(estoqueProduto);

        verify(estoqueProdutoMapper, times(1)).inserir(estoqueProduto);
    }

    @Test
    public void deveTrazerProdutosCadastrados() {
        final EstoqueProduto estoqueProduto = new EstoqueProduto(estoque, produto);
        estoqueProduto.setQuantidade(10);

        when(estoqueProdutoMapper.findByEstoqueEProduto(estoque, produto)).thenReturn(Optional.ofNullable(estoqueProduto));

        final Optional<EstoqueProduto> produtoEstoqueResultado = estoqueProdutoService.findByEstoqueEProduto(estoque, produto);

        assertThat(produtoEstoqueResultado).contains(estoqueProduto);

        verify(estoqueProdutoMapper, times(1)).findByEstoqueEProduto(estoque, produto);
    }

    @Test
    public void deveCriarEstoqueDeProdutoQuandoNaoExistir() {
        when(estoqueProdutoMapper.findByEstoqueEProduto(estoque, produto)).thenReturn(Optional.ofNullable(null));

        final EstoqueProduto estoqueProdutoFinal = estoqueProdutoService.getOrCreateByEstoqueEProduto(estoque, produto);

        verify(estoqueProdutoMapper, times(1)).inserir(estoqueProdutoFinal);

        assertThat(estoqueProdutoFinal.getQuantidade()).isZero();
    }

    @Test
    public void deveTrazerProdutoQuandoExistir() {
        final EstoqueProduto estoqueProduto = new EstoqueProduto(estoque, produto);
        estoqueProduto.setQuantidade(10);

        when(estoqueProdutoMapper.findByEstoqueEProduto(estoque, produto)).thenReturn(Optional.ofNullable(estoqueProduto));

        assertThat(estoqueProduto.getQuantidade()).isEqualTo(10);
    }

    @Test
    public void deveAtualizarAQuantidade() {
        final EstoqueProduto estoqueProduto = new EstoqueProduto(estoque, produto);

        estoqueProdutoService.atualizarQuantidade(estoqueProduto);

        verify(estoqueProdutoMapper).atualizarQuantidade(estoqueProduto);
    }

}