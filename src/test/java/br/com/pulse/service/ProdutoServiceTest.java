package br.com.pulse.service;

import br.com.pulse.mapper.ProdutoMapper;
import br.com.pulse.model.Produto;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.BDDMockito.*;
import static org.mockito.BDDMockito.given;

public class ProdutoServiceTest {

    private ProdutoService produtoService;

    private ProdutoMapper produtoMapperMock;

    @Before
    public void setup() {
        produtoMapperMock = mock(ProdutoMapper.class);
        produtoService = new ProdutoService(produtoMapperMock);
    }

    @Test
    public void deveRetornarTodosOsProdutos() {
        Produto produto1 = new Produto();
        produto1.setId(1l);
        Produto produto2 = new Produto();
        produto2.setId(2l);

        final List<Produto> produtosEsperados = Arrays.asList(produto1, produto2);

        given(produtoMapperMock.findAll()).willReturn(produtosEsperados);

        final List<Produto> produtos = produtoService.findAll();

        assertThat(produtos.size()).isEqualTo(produtosEsperados.size());
        assertThat(produtos).first().isEqualTo(produtosEsperados.get(0));
        assertThat(produtos).contains(produtosEsperados.get(1));
    }

    @Test
    public void deveRetornarTodosOsProdutosPorId() {
        Produto produto1 = new Produto();
        produto1.setId(1l);
        Produto produto2 = new Produto();
        produto2.setId(2l);

        final List<Produto> produtosEsperados = Arrays.asList(produto1, produto2);

        given(produtoMapperMock.findAllById(Arrays.asList(1l, 2l))).willReturn(produtosEsperados);

        final List<Produto> produtos = produtoService.findAllById(1l, 2l);

        assertThat(produtos.size()).isEqualTo(produtosEsperados.size());
        assertThat(produtos).first().isEqualTo(produtosEsperados.get(0));
        assertThat(produtos).contains(produtosEsperados.get(1));
    }

    @Test
    public void deveRetornarProdutoPorId() {
        final Produto produtoEsperado = new Produto();
        produtoEsperado.setId(1l);

        given(produtoMapperMock.findById(1l)).willReturn(Optional.ofNullable(produtoEsperado));

        final Optional<Produto> produto = produtoService.findById(1l);

        assertThat(produto).contains(produtoEsperado);
    }

    @Test
    public void deveCriarProduto() {
        final Produto produto = new Produto();

        produtoService.inserir(produto);

        verify(produtoMapperMock, times(1)).inserir(produto);
    }

}