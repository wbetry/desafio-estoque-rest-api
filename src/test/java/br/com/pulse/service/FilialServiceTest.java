package br.com.pulse.service;

import br.com.pulse.mapper.FilialMapper;
import br.com.pulse.model.Estoque;
import br.com.pulse.model.Filial;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

public class FilialServiceTest {

    private FilialService filialService;
    private FilialMapper filialMapper;
    private EstoqueService estoqueService;

    @Before
    public void setup() {
        filialMapper = mock(FilialMapper.class);
        estoqueService = mock(EstoqueService.class);
        filialService = new FilialService(filialMapper, estoqueService);
    }

    @Test
    public void deveInserirFilial() {
        Filial filial = new Filial();
        filial.setCnpj("03995515001309");
        filial.setRazaoSocial("Mateus Supermercados S.a");

        Estoque estoque = new Estoque();
        estoque.setFilial(filial);

        filialService.insertFilial(filial);

        verify(filialMapper).insert(filial);
    }

    @Test
    public void deveBuscarTodasAsFiliaisCadastradas() {
        Filial filial1 = new Filial();
        filial1.setId(1l);
        Filial filial2 = new Filial();
        filial2.setId(2l);

        final List<Filial> todasAsFiliaisDoBanco = Arrays.asList(filial1, filial2);

        when(filialMapper.findAll()).thenReturn(todasAsFiliaisDoBanco);

        final List<Filial> filialsEncontradas = filialService.findAll();

        assertThat(filialsEncontradas.size()).isEqualTo(2);
        assertThat(filialsEncontradas).isEqualTo(todasAsFiliaisDoBanco);
    }

}
