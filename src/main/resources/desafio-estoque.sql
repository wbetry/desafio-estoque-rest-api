create table filial (
    id              bigint generated always as identity,
    nome            varchar not null,
    cnpj            int not null,
    primary key (id)
);

create table produto (
    id              bigint generated always as identity,
    nome            varchar not null,
    codigo          bigint not null,
    descricao       text,
    primary key (id)
);

create table estoque (
    id              bigint generated always as identity,
    filial_id       bigint not null,
    primary key (id),
    foreign key (filial_id)         references filial (id)
);

create table produto_estoque (
    id              bigint generated always as identity,
    quantidade      int not null check (quantidade >= 0),
    produto_id      bigint not null,
    estoque_id      bigint not null,
    foreign key (produto_id)        references produto (id),
    foreign key (estoque_id)        references estoque (id)
);

create table pedido (
    id              bigint generated always as identity,
    estoque_id      bigint not null,
    tipo_pedido     int,
    primary key (id),
    foreign key (estoque_id)        references estoque(id)
);

create table item_pedido (
    pedido_id       bigint,
    produto_id      bigint,
    quantidade      int check (quantidade >= 0),
    primary key (pedido_id, produto_id),
    foreign key (pedido_id)         references pedido (id),
    foreign key (produto_id)        references produto (id)
);

alter table filial rename column nome to razao_social;

alter table filial alter column cnpj type varchar;

alter table pedido alter column tipo_pedido type varchar;

alter table item_pedido add column status_pedido varchar;

alter table item_pedido rename column status_pedido to status_item_pedido;





