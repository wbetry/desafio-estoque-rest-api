
INSERT INTO filial
    (razao_social, cnpj)
VALUES
    ('Mateus Supermercados S.a Loja 01', '12345678910123'),
    ('Mateus Supermercados S.a Loja 02', '12345678910124'),
    ('Mateus Supermercados S.a Loja 03', '12345678910125'),
    ('Mateus Supermercados S.a Loja 04', '12345678910126'),
    ('Mateus Supermercados S.a Loja 05', '12345678910127')
;

INSERT INTO estoque
    (filial_id)
VALUES
    (1),
    (2),
    (3),
    (4),
    (5)
;

INSERT INTO produto
    (nome, codigo, descricao)
VALUES
    ('Arroz', 1, 'Arroz'),
    ('Feijão', 2, 'Feijão'),
    ('Macarrão', 3, 'Macarrão'),
    ('Farinha', 4, 'Farinha 1Kg'),
    ('Molho de Tomate', 5, 'Molho'),
    ('Óleo', 6, 'Óleo'),
    ('Vinagre', 7, 'Vinagre'),
    ('Biscoito', 8, 'Biscoito'),
    ('Bolacha', 9, 'Bolacha'),
    ('Todynho', 10, 'Todynho'),
    ('Nescau', 11, 'Nescau')
;

INSERT INTO produto_estoque
    (produto_id, estoque_id, quantidade)
VALUES
    (1, 1, 1000),
    (2, 1, 1000),
    (3, 1, 500),
    (4, 1, 500),
    (5, 1, 500),
    (6, 1, 500),
    (5, 1, 500),
    (7, 1, 500),
    (8, 1, 500),
    (9, 1, 500),
    (10, 1, 10),
    (11, 1, 90)
;
