package br.com.pulse.mapper;

import br.com.pulse.model.Produto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface ProdutoMapper {

    List<Produto> findAll();

    List<Produto> findAllById(@Param("ids") List<Long> ids);

    Optional<Produto> findById(Long id);

    void inserir(Produto produto);

}