package br.com.pulse.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import br.com.pulse.model.PedidoEstoque;

@Mapper
@Repository
public interface PedidoMapper {

    void insert(@Param("pedido") PedidoEstoque pedido);

    void atualizarStatus(@Param("pedido") PedidoEstoque pedido);

}
