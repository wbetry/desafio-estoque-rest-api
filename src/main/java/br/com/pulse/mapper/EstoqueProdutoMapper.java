package br.com.pulse.mapper;

import br.com.pulse.model.Estoque;
import br.com.pulse.model.EstoqueProduto;
import br.com.pulse.model.Produto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Mapper
@Repository
public interface EstoqueProdutoMapper {

    Optional<EstoqueProduto> findByEstoqueEProduto(@Param("estoque") Estoque estoque, @Param("produto") Produto produto);

    Optional<EstoqueProduto> findByEstoqueId(@Param("estoqueId") Long estoqueId);

    void inserir(@Param("estoqueProduto") EstoqueProduto estoqueProduto);

    void atualizarQuantidade(@Param("estoqueProduto") EstoqueProduto estoqueProduto);

}
