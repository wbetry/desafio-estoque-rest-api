package br.com.pulse.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import br.com.pulse.model.ItemPedidoEstoque;

@Mapper
@Repository
public interface ItemPedidoMapper {

    void inserirItens(@Param("itens") List<ItemPedidoEstoque> itens);

    void atualizarStatus(@Param("item") ItemPedidoEstoque item);

}
