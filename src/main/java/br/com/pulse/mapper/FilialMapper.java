package br.com.pulse.mapper;

import br.com.pulse.model.Filial;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface FilialMapper {

    List<Filial> findAll();

    Optional<Filial> findById(@Param("id") Long id);

    void insert(@Param("filial") Filial filial);

}
