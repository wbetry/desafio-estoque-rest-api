package br.com.pulse.mapper;

import br.com.pulse.model.Estoque;
import br.com.pulse.model.Filial;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Mapper
@Repository
public interface EstoqueMapper {

    Optional<Estoque> findById(@Param("id") Long id);

    Optional<Estoque> findByFilial(@Param("filial") Filial filial);

    void insert(@Param("estoque") Estoque estoque);

}
