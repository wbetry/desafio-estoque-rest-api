package br.com.pulse.model;

import br.com.pulse.model.enums.StatusItemPedido;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "pedido", "produto"})
public class ItemPedidoEstoque {

    private PedidoEstoque pedido;
    private Produto produto;
    private Integer quantidade;
    private StatusItemPedido statusItemPedido;

    public ItemPedidoEstoque() {

    }

    public ItemPedidoEstoque(PedidoEstoque pedido, Produto produto) {
        this.pedido = pedido;
        this.produto = produto;
    }

    public void adicionaQuantidade(Integer quantidade) {
        this.quantidade += quantidade;
    }

}
