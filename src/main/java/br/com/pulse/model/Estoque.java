package br.com.pulse.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = {"id"})
public final class Estoque {

    private Long id;
    private Filial filial;
    private List<EstoqueProduto> estoqueProdutos = new ArrayList<>();

}
