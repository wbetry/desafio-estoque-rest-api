package br.com.pulse.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum StatusPedido {

    INICIADO(0),
    FINALIZADO(1);

    private final Integer status;

}
