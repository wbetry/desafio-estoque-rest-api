package br.com.pulse.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum StatusItemPedido {

    INICIADO(0),
    PROCESSADO(1),
    ERRO_ESTOQUE_INSUFICIENTE(2),
    ERRO_PRODUTO_INEXISTENTE(3),
    ERRO_QUANTIDADE_NEGATIVA(4);

    private final Integer status;

}
