package br.com.pulse.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoPedido {

    ENTRADA(0), SAIDA(1);

    private final Integer tipo;

}
