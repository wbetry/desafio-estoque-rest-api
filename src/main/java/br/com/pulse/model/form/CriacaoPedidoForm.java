package br.com.pulse.model.form;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import br.com.pulse.model.Estoque;
import br.com.pulse.model.ItemPedidoEstoque;
import br.com.pulse.model.PedidoEstoque;
import br.com.pulse.model.Produto;
import br.com.pulse.model.enums.TipoPedido;
import lombok.Data;

@Data
public class CriacaoPedidoForm {

    @NotNull
    private TipoPedido tipoPedido;
    @NotNull
    private Long estoqueId;
    @NotEmpty
    private List<@NotNull ItemPedidoInternalForm> itensPedido;

    public PedidoEstoque convert() {
        final PedidoEstoque pedido = new PedidoEstoque();
        final Estoque estoque = new Estoque();
        estoque.setId(estoqueId);

        pedido.setTipoPedido(tipoPedido);
        pedido.setEstoque(estoque);
        pedido.adicionar(itensPedido.stream().map(ItemPedidoInternalForm::convert).toArray(ItemPedidoEstoque[]::new));

        return pedido;
    }

    @Data
    public static class ItemPedidoInternalForm {

        @NotNull
        private Long produtoId;
        @NotNull
        @Positive
        private Integer quantidade;

        public ItemPedidoEstoque convert() {
            ItemPedidoEstoque item = new ItemPedidoEstoque();

            Produto produto = new Produto();
            produto.setId(produtoId);

            item.setProduto(produto);
            item.setQuantidade(quantidade);

            return item;
        }

    }

}
