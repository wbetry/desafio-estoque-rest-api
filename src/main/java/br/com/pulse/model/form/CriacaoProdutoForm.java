package br.com.pulse.model.form;

import br.com.pulse.model.Produto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CriacaoProdutoForm {

    @NotBlank
    private String nome;
    @NotNull
    private Long codigo;
    @NotBlank
    private String descricao;

    public Produto convert() {
        Produto produto = new Produto();
        produto.setNome(nome);
        produto.setCodigo(codigo);
        produto.setDescricao(descricao);

        return  produto;
    }
}
