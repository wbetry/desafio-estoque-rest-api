package br.com.pulse.model.form;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import br.com.pulse.model.Filial;
import lombok.Data;

@Data
public class FilialForm {

    @NotBlank
    private String razaoSocial;
    @NotNull
    private String cnpj;

    public Filial convert() {
        final Filial filial = new Filial();
        filial.setRazaoSocial(razaoSocial);
        filial.setCnpj(cnpj);

        return filial;
    }

    public String setCnpj(String cnpj) {
        return cnpj.replace(".", "")
                .replace("-", "")
                .replace("/", "");
    }

}
