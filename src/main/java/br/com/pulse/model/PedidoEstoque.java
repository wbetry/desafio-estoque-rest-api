package br.com.pulse.model;

import java.util.*;

import br.com.pulse.model.enums.StatusPedido;
import br.com.pulse.model.enums.TipoPedido;
import lombok.Data;

@Data
public class PedidoEstoque {

    private Long id;
    private TipoPedido tipoPedido;
    private final List<ItemPedidoEstoque> itens = new ArrayList<>();
    private Estoque estoque;
    private StatusPedido statusPedido;


    public void adicionar(ItemPedidoEstoque... itensInsercao) {
        final List<ItemPedidoEstoque> listaInsercao = Arrays.asList(itensInsercao);

        for (ItemPedidoEstoque item : listaInsercao) {
            if (!itens.contains(item))  {
                itens.add(item);
                continue;
            }

            for (ItemPedidoEstoque itemIgual : itens) {
                if (itemIgual.equals(item)) {
                    itemIgual.adicionaQuantidade(item.getQuantidade());
                }
            }
        }
    }

}
