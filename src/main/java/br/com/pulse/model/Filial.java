package br.com.pulse.model;

import lombok.Data;
import org.apache.commons.lang.StringUtils;

@Data
public class Filial {

    private Long id;
    private String razaoSocial;
    private String cnpj;

    public String getCnpjFormatado() {
        return StringUtils.leftPad(cnpj, 14, "0");
    }

}
