package br.com.pulse.model.dto;

import br.com.pulse.model.Filial;
import lombok.Data;

@Data
public class FilialDTO {

    private Long id;
    private String razaoSocial;
    private String cnpj;

    public FilialDTO(Filial filial) {
        id = filial.getId();
        razaoSocial = filial.getRazaoSocial();
        cnpj = filial.getCnpjFormatado();
    }

}
