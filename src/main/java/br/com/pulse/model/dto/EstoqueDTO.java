package br.com.pulse.model.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import br.com.pulse.model.Estoque;
import br.com.pulse.model.EstoqueProduto;
import lombok.Getter;

@Getter
public final class EstoqueDTO {

    private final Long id;
    private final FilialDTO filial;
    private final List<EstoqueProdutoInternalDTO> estoqueProdutos = new ArrayList<>();

    public EstoqueDTO(Estoque estoque) {
        id = estoque.getId();
        filial = new FilialDTO(estoque.getFilial());
        estoqueProdutos.addAll(estoque.getEstoqueProdutos().stream().map(EstoqueProdutoInternalDTO::new).collect(Collectors.toList()));
    }

    @Getter
    public static class EstoqueProdutoInternalDTO {

        private final Long id;
        private final Integer quantidade;
        private final ProdutoDTO produto;

        public EstoqueProdutoInternalDTO(EstoqueProduto estoqueProduto) {
            id = estoqueProduto.getId();
            quantidade = estoqueProduto.getQuantidade();
            produto = new ProdutoDTO(estoqueProduto.getProduto());
        }

    }

}
