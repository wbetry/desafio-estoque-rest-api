package br.com.pulse.model.dto;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

import br.com.pulse.model.*;
import br.com.pulse.model.enums.TipoPedido;
import lombok.Getter;

@Getter
public class PedidoDTO {

    private Long id;
    private TipoPedido tipoPedido;
    private final Set<ItemPedidoInternalDTO> itens = new LinkedHashSet<>();

    public PedidoDTO(PedidoEstoque pedido) {
        id = pedido.getId();
        tipoPedido = pedido.getTipoPedido();
        itens.addAll(pedido.getItens().stream().map(ItemPedidoInternalDTO::new).collect(Collectors.toSet()));
    }

    @Getter
    public static class ItemPedidoInternalDTO {

        private Long produtoId;
        private String nomeProduto;
        private Long codigoProduto;
        private Integer quantidade;

        public ItemPedidoInternalDTO(ItemPedidoEstoque item) {
            final Produto produto = item.getProduto();
            produtoId = produto.getId();
            nomeProduto = produto.getNome();
            codigoProduto = produto.getCodigo();
            quantidade = item.getQuantidade();
        }

    }

}
