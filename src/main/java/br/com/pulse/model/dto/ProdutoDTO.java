package br.com.pulse.model.dto;

import br.com.pulse.model.Produto;
import lombok.Data;

@Data
public class ProdutoDTO {

    private Long id;
    private String nome;
    private Long codigo;
    private String descricao;

    public ProdutoDTO(Produto produto) {
        id = produto.getId();
        nome = produto.getNome();
        codigo = produto.getCodigo();
        descricao = produto.getDescricao();
    }

}
