package br.com.pulse.model.exception;

public class EstoqueException extends RuntimeException {

    private static final long serialVersionUID = -8569397946270793066L;

    public EstoqueException(String message) {
        super(message);
    }

    public EstoqueException(String message, Throwable trace) {
        super(message, trace);
    }

}
