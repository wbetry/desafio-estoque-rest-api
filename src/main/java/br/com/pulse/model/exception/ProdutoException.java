package br.com.pulse.model.exception;

final public class ProdutoException extends RuntimeException {

    private static final long serialVersionUID = 5886342392715611817L;

    public ProdutoException(String message) {
        super(message);
    }

    public ProdutoException(String message, Throwable trace) {
        super(message, trace);
    }

}
