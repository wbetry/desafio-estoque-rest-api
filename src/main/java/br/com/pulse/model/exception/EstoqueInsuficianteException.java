package br.com.pulse.model.exception;

final public class EstoqueInsuficianteException extends EstoqueException {

    private static final long serialVersionUID = 9121626255550709275L;

    public EstoqueInsuficianteException(String message) {
        super(message);
    }

    public EstoqueInsuficianteException(String message, Throwable trace) {
        super(message, trace);
    }

}
