package br.com.pulse.model;

import br.com.pulse.model.enums.TipoPedido;
import br.com.pulse.model.exception.EstoqueException;
import br.com.pulse.model.exception.EstoqueInsuficianteException;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(of = { "estoque", "produto" })
public class EstoqueProduto {

    private Long id;
    private Integer quantidade;
    private Estoque estoque;
    private Produto produto;

    public EstoqueProduto() {

    }

    public EstoqueProduto(Estoque estoque, Produto produto) {
        this.estoque = estoque;
        this.produto = produto;
    }

    public void adicionarEstoque(Integer quantidade) {
        if (quantidade <= 0) {
            throw new EstoqueException("Só pode adicionar produtos com quantidades positivas.");
        }

        this.quantidade += quantidade;
    }

    public void removerEstoque(Integer quantidade) {
        if (quantidade <= 0) {
            throw new EstoqueException("Só pode remover produtos com quantidades positivas.");
        }

        if (quantidade > this.quantidade) {
            throw new EstoqueInsuficianteException("Estoque com quantidade de produtos insuficiente.");
        }

        this.quantidade -= quantidade;
    }

    public void movimentarEstoque(TipoPedido tipoPedido, Integer quantidade) {
        if (tipoPedido.equals(TipoPedido.ENTRADA)) {
            this.adicionarEstoque(quantidade);
        }

        if (tipoPedido.equals(TipoPedido.SAIDA)) {
            this.removerEstoque(quantidade);
        }
    }

}
