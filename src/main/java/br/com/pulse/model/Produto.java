package br.com.pulse.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = {"id"})
public class Produto {

    private Long id;
    private String nome;
    private Long codigo;
    private String descricao;

}
