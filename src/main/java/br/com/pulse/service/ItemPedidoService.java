package br.com.pulse.service;

import java.util.List;
import java.util.stream.Collectors;

import br.com.pulse.model.Produto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.pulse.mapper.ItemPedidoMapper;
import br.com.pulse.model.ItemPedidoEstoque;

@Service
public class ItemPedidoService {

    private final ItemPedidoMapper itemPedidoMapper;
    private final ProdutoService produtoService;

    public ItemPedidoService(ItemPedidoMapper itemPedidoMapper, ProdutoService produtoService) {
        this.itemPedidoMapper = itemPedidoMapper;
        this.produtoService = produtoService;
    }

    @Transactional
    public void inserirItens(List<ItemPedidoEstoque> itens) {
        itemPedidoMapper.inserirItens(itens);
    }

    public void atualizarStatus(ItemPedidoEstoque item) {
        itemPedidoMapper.atualizarStatus(item);
    }

    public List<ItemPedidoEstoque> itensValidos(List<ItemPedidoEstoque> itens) {
        final Long[] produtosId = idsDosProdutosDosItens(itens);

        final List<Produto> produtos = produtoService.findAllById(produtosId);

        return itens.stream()
                .filter(item -> produtos.contains(item.getProduto()))
                .collect(Collectors.toList());
    }

    private Long[] idsDosProdutosDosItens(List<ItemPedidoEstoque> itens) {
        return itens.stream()
                .map(ItemPedidoEstoque::getProduto)
                .map(Produto::getId)
                .toArray(Long[]::new);
    }

}
