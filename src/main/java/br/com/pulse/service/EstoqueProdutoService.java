package br.com.pulse.service;

import br.com.pulse.mapper.EstoqueProdutoMapper;
import br.com.pulse.model.Estoque;
import br.com.pulse.model.EstoqueProduto;
import br.com.pulse.model.Produto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class EstoqueProdutoService {

    private final EstoqueProdutoMapper estoqueProdutoMapper;

    public EstoqueProdutoService(EstoqueProdutoMapper estoqueProdutoMapper) {
        this.estoqueProdutoMapper = estoqueProdutoMapper;
    }

    public Optional<EstoqueProduto> findByEstoqueEProduto(Estoque estoque, Produto produto) {
        return estoqueProdutoMapper.findByEstoqueEProduto(estoque, produto);
    }

    @Transactional
    public EstoqueProduto getOrCreateByEstoqueEProduto(Estoque estoque, Produto produto) {
        final Optional<EstoqueProduto> estoqueProdutoOptional = findByEstoqueEProduto(estoque, produto);

        if (!estoqueProdutoOptional.isPresent()) {
            final EstoqueProduto estoqueProduto = new EstoqueProduto(estoque, produto);
            estoqueProduto.setQuantidade(0);
            inserir(estoqueProduto);

            return estoqueProduto;
        }

        return estoqueProdutoOptional.get();
    }

    @Transactional
    public EstoqueProduto inserir(EstoqueProduto estoqueProduto) {
        estoqueProdutoMapper.inserir(estoqueProduto);
        return estoqueProduto;
    }

    @Transactional
    public void atualizarQuantidade(EstoqueProduto estoque) {
        estoqueProdutoMapper.atualizarQuantidade(estoque);
    }

}
