package br.com.pulse.service;

import br.com.pulse.mapper.FilialMapper;
import br.com.pulse.model.Estoque;
import br.com.pulse.model.Filial;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class FilialService {

    private final FilialMapper filialMapper;
    private final EstoqueService estoqueService;

    public FilialService(FilialMapper filialMapper, EstoqueService estoqueService) {
        this.filialMapper = filialMapper;
        this.estoqueService = estoqueService;
    }

    public List<Filial> findAll() {
        return filialMapper.findAll();
    }

    public Optional<Filial> findById(Long id) {
        return filialMapper.findById(id);
    }

    @Transactional
    public void insertFilial(Filial filial) {
        filialMapper.insert(filial);
        final Estoque estoque = new Estoque();
        estoque.setFilial(filial);
        estoqueService.insert(estoque);
    }

}
