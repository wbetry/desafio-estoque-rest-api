package br.com.pulse.service;

import br.com.pulse.model.*;
import br.com.pulse.model.enums.StatusItemPedido;
import br.com.pulse.model.exception.EstoqueException;
import br.com.pulse.model.exception.EstoqueInsuficianteException;
import br.com.pulse.model.exception.ProdutoException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.pulse.mapper.PedidoMapper;
import br.com.pulse.model.enums.StatusPedido;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class PedidoService {

    private final PedidoMapper pedidoMapper;
    private final ItemPedidoService itemPedidoService;
    private final EstoqueService estoqueService;
    private final EstoqueProdutoService estoqueProdutoService;

    public PedidoService(
            PedidoMapper pedidoMapper,
            ItemPedidoService itemPedidoService,
            EstoqueService estoqueService,
            EstoqueProdutoService estoqueProdutoService) {
        this.pedidoMapper = pedidoMapper;
        this.itemPedidoService = itemPedidoService;
        this.estoqueService = estoqueService;
        this.estoqueProdutoService = estoqueProdutoService;
    }

    @Transactional
    public PedidoEstoque novoPedido(PedidoEstoque pedido) {
        pedido.setStatusPedido(StatusPedido.INICIADO);

        final Optional<Estoque> estoqueOp = estoqueService.findById(pedido.getEstoque().getId());

        if (!estoqueOp.isPresent()) {
            throw new EstoqueException("Filial não encontrada");
        }

        final List<ItemPedidoEstoque> itensOk = itemPedidoService.itensValidos(pedido.getItens());

        pedidoMapper.insert(pedido);

        itensOk.forEach(it -> it.setPedido(pedido));

        itemPedidoService.inserirItens(itensOk);

        itensOk.forEach(item -> {
            final EstoqueProduto estoqueProduto = estoqueProdutoService.getOrCreateByEstoqueEProduto(estoqueOp.get(), item.getProduto());

            try {
                estoqueProduto.movimentarEstoque(pedido.getTipoPedido(), item.getQuantidade());
                estoqueProdutoService.atualizarQuantidade(estoqueProduto);
                item.setStatusItemPedido(StatusItemPedido.PROCESSADO);
            } catch (EstoqueInsuficianteException e) {
                item.setStatusItemPedido(StatusItemPedido.ERRO_ESTOQUE_INSUFICIENTE);
                log.error("Item Pedido: {} Produto: {} com erro {}", pedido.getId(), item.getProduto(), e.getMessage());
            } catch (ProdutoException e) {
                item.setStatusItemPedido(StatusItemPedido.ERRO_PRODUTO_INEXISTENTE);
                log.error("Item Pedido: {} Produto: {} com erro {}", pedido.getId(), item.getProduto(), e.getMessage());
            }

            itemPedidoService.atualizarStatus(item);
        });

        pedido.setStatusPedido(StatusPedido.FINALIZADO);
        pedidoMapper.atualizarStatus(pedido);

        return pedido;
    }

}
