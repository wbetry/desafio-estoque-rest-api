package br.com.pulse.service;

import br.com.pulse.mapper.ProdutoMapper;
import br.com.pulse.model.Produto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    private final ProdutoMapper produtoMapper;

    public ProdutoService(ProdutoMapper produtoMapper) {
        this.produtoMapper = produtoMapper;
    }

    public List<Produto> findAll() {
        return produtoMapper.findAll();
    }

    public List<Produto> findAllById(Long... produtosId) {
        List<Long> ids = Arrays.asList(produtosId);

        return produtoMapper.findAllById(ids);
    }

    public Optional<Produto> findById(Long id) {
        return produtoMapper.findById(id);
    }

    @Transactional
    public void inserir(Produto produto) {
        produtoMapper.inserir(produto);
    }
}
