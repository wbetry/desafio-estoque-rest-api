package br.com.pulse.service;

import br.com.pulse.mapper.EstoqueMapper;
import br.com.pulse.model.Estoque;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class EstoqueService {

    private final EstoqueMapper estoqueMapper;

    public EstoqueService(EstoqueMapper estoqueMapper) {
        this.estoqueMapper = estoqueMapper;
    }

    public Optional<Estoque> findById(Long id) {
        return estoqueMapper.findById(id);
    }

    @Transactional
    public void insert(Estoque estoque) {
        estoqueMapper.insert(estoque);
    }

}
