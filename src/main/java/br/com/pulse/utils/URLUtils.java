package br.com.pulse.utils;

import java.net.URI;
import java.text.MessageFormat;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class URLUtils {

    public URI generateForResource(Long id) {
        final HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        final String concatId = MessageFormat.format("/{0}", id);
        return URI.create(request.getRequestURL().append(concatId).toString());
    }

}
