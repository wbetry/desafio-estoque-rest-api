package br.com.pulse.support;

import lombok.Getter;

@Getter
public class ExceptionResponse {

    private final String erro;


    public ExceptionResponse(String erro) {
        this.erro = erro;
    }

}
