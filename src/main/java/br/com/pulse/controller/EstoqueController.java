package br.com.pulse.controller;

import br.com.pulse.model.Estoque;
import br.com.pulse.model.dto.EstoqueDTO;
import br.com.pulse.service.EstoqueService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/estoques")
class EstoqueController {

    private final EstoqueService estoqueService;

    public EstoqueController(EstoqueService estoqueService) {
        this.estoqueService = estoqueService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<EstoqueDTO> porId(@PathVariable Long id) {
        final Optional<Estoque> estoqueOp = estoqueService.findById(id);

        return ResponseEntity.of(estoqueOp.map(EstoqueDTO::new));
    }

}
