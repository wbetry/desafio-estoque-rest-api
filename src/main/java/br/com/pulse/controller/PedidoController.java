package br.com.pulse.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.pulse.model.PedidoEstoque;
import br.com.pulse.model.dto.PedidoDTO;
import br.com.pulse.model.form.CriacaoPedidoForm;
import br.com.pulse.service.PedidoService;
import br.com.pulse.utils.URLUtils;

@RestController
@RequestMapping("/pedidos")
public class PedidoController {

    private final PedidoService pedidoService;

    public PedidoController(PedidoService pedidoService) {
        this.pedidoService = pedidoService;
    }

    @PostMapping
    public ResponseEntity<PedidoDTO> novoPedido(@Valid @RequestBody CriacaoPedidoForm form) {
        final PedidoEstoque pedido = form.convert();

        pedidoService.novoPedido(pedido);

        URI location = new URLUtils().generateForResource(pedido.getId());

        return ResponseEntity.created(location).body(new PedidoDTO(pedido));
    }

}
