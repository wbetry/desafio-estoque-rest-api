package br.com.pulse.controller;

import br.com.pulse.model.Produto;
import br.com.pulse.model.dto.ProdutoDTO;
import br.com.pulse.model.form.CriacaoProdutoForm;
import br.com.pulse.service.ProdutoService;
import br.com.pulse.utils.URLUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/produtos")
@RequiredArgsConstructor
public class ProdutoController {

    private final ProdutoService produtoService;

    @GetMapping
    public List<ProdutoDTO> findAll() {
        final List<Produto> produtos = produtoService.findAll();
        return produtos.stream().map(ProdutoDTO::new).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProdutoDTO> findById(@PathVariable Long id) {
        final Optional<Produto> produtoOptional = produtoService.findById(id);
        return ResponseEntity.of(produtoOptional.map(ProdutoDTO::new));
    }

    @PostMapping
    public ResponseEntity<ProdutoDTO> inserir(@RequestBody CriacaoProdutoForm form) {
        final Produto produto = form.convert();
        produtoService.inserir(produto);

        final URI createdResource = new URLUtils().generateForResource(produto.getId());

        return  ResponseEntity.created(createdResource).body(new ProdutoDTO(produto));
    }

}
