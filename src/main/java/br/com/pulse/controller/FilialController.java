package br.com.pulse.controller;


import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.pulse.model.Filial;
import br.com.pulse.model.dto.FilialDTO;
import br.com.pulse.model.form.FilialForm;
import br.com.pulse.service.FilialService;
import br.com.pulse.utils.URLUtils;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/filiais")
@RequiredArgsConstructor
public class FilialController {

    private final FilialService filialService;

    @GetMapping
    public List<FilialDTO> findAll() {
        return  filialService.findAll().stream().map(FilialDTO::new).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<FilialDTO> findById(@PathVariable Long id) {
        final Optional<Filial> filialOptional = filialService.findById(id);

        return ResponseEntity.of(filialOptional.map(FilialDTO::new));
    }

    @PostMapping
    public ResponseEntity<FilialDTO> insertFilial(@Valid @RequestBody FilialForm form) {
        Filial filial = form.convert();

        filialService.insertFilial(filial);

        URI url = new URLUtils().generateForResource(filial.getId());

        return ResponseEntity.created(url).body(new FilialDTO(filial));
    }

}
